//
//  ThirdViewController.swift
//  MyAssessment
//
//  Created by Boon Earn Ng on 16/4/15.
//  Copyright (c) 2015 Boon Earn Ng. All rights reserved.
//

import UIKit

class ThirdViewController: UITableViewController {
    
    var dateList: NSMutableArray!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init
        dateList = NSMutableArray()
        
        setupDateList();
    }

    func setupDateList() {
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        let date = NSDate();
        let calendar = NSCalendar.currentCalendar()
        let dayComponent = NSDateComponents()
        
        for var index = 0; index < 12; index++ {
            dayComponent.day = index;
            let newDate = calendar.dateByAddingComponents(dayComponent, toDate: date, options:nil)!
            let dateString = dateFormatter.stringFromDate(newDate)
            dateList.addObject(dateString)
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return dateList.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DateCell", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...
        cell.textLabel?.text = dateList.objectAtIndex(indexPath.row) as? String

        return cell
    }


}
