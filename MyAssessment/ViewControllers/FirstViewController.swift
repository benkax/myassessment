//
//  FirstViewController.swift
//  MyAssessment
//
//  Created by Boon Earn Ng on 16/4/15.
//  Copyright (c) 2015 Boon Earn Ng. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,SecondViewControllerDelegate {
    
    @IBOutlet weak var buttonPressedLabel: UILabel!
    
    enum Color: NSInteger {
        case Blue = 1
        case Red = 2
        case Green = 3
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func colorButtonPressed(sender: UIButton) {
        self.performSegueWithIdentifier("showSecondViewController", sender: sender)
    }
    
    func numberButtonPressed(numberString:String?) {
        if(numberString != nil){
            buttonPressedLabel.text = NSString(format: "button pressed is %@", numberString!) as String
        } else {
            buttonPressedLabel.text = "None"
        }
    }

    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "showSecondViewController") {
            let myDestinationViewController = segue.destinationViewController as! SecondViewController
            var tag:NSInteger = sender!.tag;
            
            myDestinationViewController.buttonPressedInteger = tag
            myDestinationViewController.delegate = self
            
            switch tag {
            case Color.Blue.rawValue:
                //BlueColor is too dark, change to a lighter blue
                let color = UIColor (red: 52/255, green: 152/255,  blue: 219/255, alpha: 1.0)
                myDestinationViewController.backgroundColor = color
                
            case Color.Red.rawValue:
                
                myDestinationViewController.backgroundColor = UIColor.redColor();
                
            case Color.Green.rawValue:
                
                myDestinationViewController.backgroundColor = UIColor.greenColor();
                
            default:
                break
            }
        }
    }
}
