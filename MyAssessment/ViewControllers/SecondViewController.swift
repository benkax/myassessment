//
//  SecondViewController.swift
//  MyAssessment
//
//  Created by Boon Earn Ng on 16/4/15.
//  Copyright (c) 2015 Boon Earn Ng. All rights reserved.
//

import UIKit

protocol SecondViewControllerDelegate{
    func numberButtonPressed(numberString:String?)
}

enum Number: NSInteger {
    case One = 1
    case Two = 2
    case Three = 3
}

class SecondViewController: UIViewController {
    
    @IBOutlet weak var buttonPressedLabel: UILabel!
    
    var delegate:SecondViewControllerDelegate! = nil
    var backgroundColor: UIColor!
    var buttonPressedInteger: NSInteger!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: "back:")
        self.navigationItem.leftBarButtonItem = newBackButton;
        
        updateViewBackgroundColor();
        updateButtonPressedIntegerLabel();
    }
    
    func updateViewBackgroundColor() {
        if((self.backgroundColor) != nil){
            self.view.backgroundColor = backgroundColor;
        }
    }
    
    func updateButtonPressedIntegerLabel() {
        if((self.buttonPressedInteger) != nil){
            buttonPressedLabel.text = NSString(format: "button pressed is %ld", self.buttonPressedInteger!) as String
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func numberButtonPressed(sender: UIButton) {
        
        if (self.delegate != nil) {
            
            var tag:NSInteger = sender.tag;
            
            switch tag {
            case Number.One.rawValue:
                delegate.numberButtonPressed("ONE")
                
            case Number.Two.rawValue:
                delegate.numberButtonPressed("TWO")
                
            case Number.Three.rawValue:
                delegate.numberButtonPressed("THREE")
                
            default:
                break
            }
            
        }
        
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func back(sender: UIBarButtonItem) {
        
        if (self.delegate != nil) {
            delegate.numberButtonPressed(nil)
        }
        
        navigationController?.popViewControllerAnimated(true)
    }
    
}
